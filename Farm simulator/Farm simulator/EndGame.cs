﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farm_simulator
{
    class EndGame
    {
        private NextTurn Turns = new NextTurn();

        public void EndGameTurn()
        {
            if(Turns.CurrentTurn >= 156) 
            {
                ShowEndGameMenu();
            }

            else { }
        }

        public void ShowEndGameMenu()
        {
            new End_Game_Menu().ShowDialog();
        }
    }
}
