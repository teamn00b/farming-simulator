﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farm_simulator
{
    public partial class Take_Loan_Menu : Form
    {
        private Bank bankClass = new Bank();
        private Main_Menu mainMenu;

        public Take_Loan_Menu(Main_Menu menuForm)
        {
            InitializeComponent();
            UpdateText();
            this.mainMenu = menuForm;
        }

        public void UpdateText()
        {
            if (bankClass.IsLoanTaken)
            {
                button_takeLoan.Enabled = false;
            }
            else if(!bankClass.IsLoanTaken)
            {
                button_takeLoan.Enabled = true;
            }

            InterestRateOnLoan.Text = bankClass.DebtInterestAmount.ToString();
        }

        private void button_takeLoan_Click(object sender, EventArgs e)
        {
            int n;
            bool isNumeric = int.TryParse(LoanTakeOut.Text.ToString(), out n);
            float amount = float.Parse(n.ToString());
            if (isNumeric)
            {
                if(amount <= 3500f)
                {
                    bankClass.TakeLoan(n);
                    UpdateText();
                    mainMenu.UpdateLabels();
                }
                else
                {
                    MessageBox.Show("Max Amount £3500");
                }
            }
            else
            {
                MessageBox.Show("Amount Entered Invalid");
            }

        }
    }
}
