﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farm_simulator
{
    public partial class Field_Information_Menu : Form
    {
        private static string fieldIDShown;

        private SelectedField selectedFieldClass = new SelectedField();
        private Stats statsClass = new Stats();
        private Field fieldClass = new Field();

        #region encapsulation
        public static string FieldIDShown
        {
            get { return Field_Information_Menu.fieldIDShown; }
            set { Field_Information_Menu.fieldIDShown = value; }
        }
        #endregion

        public Field_Information_Menu()
        {
            InitializeComponent();
            UpdateLabels();
        }

        public void UpdateLabels()
        {
            CropValue.Text = Field.fields[selectedFieldClass.FieldID].FieldCropType;
            GrowthValue.Text = (Field.fields[selectedFieldClass.FieldID].FieldGrowthProcess * 10).ToString();
            QualityValue.Text = Field.fields[selectedFieldClass.FieldID].FieldCropQuality.ToString();
            StatusValue.Text = Field.fields[selectedFieldClass.FieldID].FieldStatus;
            InfectedValue.Text = Field.fields[selectedFieldClass.FieldID].IsFieldInfected.ToString();
            FertiliserValue.Text = Field.fields[selectedFieldClass.FieldID].IsFieldFertilised.ToString();

            //Disable various button depending on conditions of the currently selected field
            if (Field.fields[selectedFieldClass.FieldID].IsFieldEmpty)
            {
                if (Field.fields[selectedFieldClass.FieldID].IsFieldFertilised)
                {
                    button_applyFertiliser.Enabled = false;
                }
                else
                {
                    if (statsClass.Fertiliser >= 1)
                    {
                        button_applyFertiliser.Enabled = true;
                    }
                    else
                    {
                        button_applyFertiliser.Enabled = false;
                    }
                }

                button_destroyCrop.Enabled = false;
                button_ApplyPesticide.Enabled = false;

                if (statsClass.CerealSeed >= 1)
                {
                    PlantCereal.Enabled = true;
                }
                else
                {
                    PlantCereal.Enabled = false;
                }

                if (statsClass.VegSeed >= 1)
                {
                    PlantVeg.Enabled = true;
                }
                else
                {
                    PlantVeg.Enabled = false;
                }

                if (statsClass.FruitSeed >= 1)
                {
                    PlantFruit.Enabled = true;
                }
                else
                {
                    PlantFruit.Enabled = false;
                }
            }
            else
            {
                button_applyFertiliser.Enabled = false;
                PlantFruit.Enabled = false;
                PlantCereal.Enabled = false;
                PlantVeg.Enabled = false;
                button_destroyCrop.Enabled = true;

                if (Field.fields[selectedFieldClass.FieldID].IsFieldInfected)
                {
                    if (statsClass.Pesticide >= 1)
                    {
                        button_ApplyPesticide.Enabled = true;
                    }
                    else
                    {
                        button_ApplyPesticide.Enabled = false;
                    }
                }
                else
                {
                    button_ApplyPesticide.Enabled = false;
                }
            }

            //If field is empty or field crop isnt yet fully grown, disable harvesting button
            if (Field.fields[selectedFieldClass.FieldID].IsFieldEmpty || !Field.fields[selectedFieldClass.FieldID].IsFieldCropFullyGrown)
            {
                button_harvestCrop.Enabled = false;
            }
            else
            {
                button_harvestCrop.Enabled = true;
            }
        }

        private void label_fieldName_Click(object sender, EventArgs e)
        {

        }

        private void Field_Information_Menu_Load(object sender, EventArgs e)
        {
            label_fieldName.Text = fieldIDShown;
        }

        private void label2_Click(object sender, EventArgs e)
        {
            
        }
        
        private void label_cropPlanted_Click(object sender, EventArgs e)
        {

        }

        private void button_sowCrop_Click(object sender, EventArgs e)
        {
            PlantCrop("Veg");
        }

        private void button_applyFertiliser_Click(object sender, EventArgs e)
        {
            fieldClass.FertiliseField();
            UpdateLabels();
        }

        private void button_ApplyPesticide_Click(object sender, EventArgs e)
        {
            fieldClass.TreatField();
            UpdateLabels();
        }

        private void button_destroyCrop_Click(object sender, EventArgs e)
        {
            fieldClass.DestroyFieldCrops();
            UpdateLabels();
        }

        private void PlantFruit_Click(object sender, EventArgs e)
        {
            PlantCrop("Fruit");
        }

        private void PlantCereal_Click(object sender, EventArgs e)
        {
            PlantCrop("Cereal");
        }

        private void button_harvestCrop_Click(object sender, EventArgs e)
        {
            fieldClass.HarvestField();
            UpdateLabels();
        }

        public void PlantCrop(string cropType)
        {
            fieldClass.PlantField(cropType);
            UpdateLabels();
        }

        private void StatusValue_Click(object sender, EventArgs e)
        {

        }
    }
}
