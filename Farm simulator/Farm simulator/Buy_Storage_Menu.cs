﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Farm_simulator
{
    public partial class Buy_Storage_Menu : Form
    {
        private Storage_Menu storage;

        private Storage storageClass = new Storage(0,0,"NoCrop");
        private Bank bankClass = new Bank();
        private Stats stats = new Stats();

        public Buy_Storage_Menu(Storage_Menu storageForm)
        {
            InitializeComponent();
            this.storage = storageForm;

            label_PriceDisplay.Text = "£" + storageClass.StoragePrice.ToString();

            if (storageClass.StoragePrice > bankClass.CurrentBalance)
            {
                button_buyCerealStorage.Enabled = false;
                button_buyFruitStorage.Enabled = false;
                button_buyVegStorage.Enabled = false;
            }
        }

        private void button_buyVegStorage_Click(object sender, EventArgs e)
        {
            storageClass.BuyStorage("Veg");
            storage.UpdateText();
            stats.UpdateMoneySpent(storageClass.StoragePrice);
            this.Hide();
        }

        private void button_buyFruitStorage_Click(object sender, EventArgs e)
        {
            storageClass.BuyStorage("Fruit");
            storage.UpdateText();
            stats.UpdateMoneySpent(storageClass.StoragePrice);
            this.Hide();
        }

        private void button_buyCerealStorage_Click(object sender, EventArgs e)
        {
            storageClass.BuyStorage("Cereal");
            storage.UpdateText();
            stats.UpdateMoneySpent(storageClass.StoragePrice);
            this.Hide();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
