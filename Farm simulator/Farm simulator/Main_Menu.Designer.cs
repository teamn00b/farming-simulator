﻿namespace Farm_simulator
{
    partial class Main_Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_Menu));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSplitButton_Storage = new System.Windows.Forms.ToolStripSplitButton();
            this.storageStatusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.destroyCropToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSplitButton_bank = new System.Windows.Forms.ToolStripSplitButton();
            this.takeLoanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.payDebtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripButton_stats = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_save = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.label_week = new System.Windows.Forms.Label();
            this.label_weekNumber = new System.Windows.Forms.Label();
            this.label_weather = new System.Windows.Forms.Label();
            this.label_weatherStatus = new System.Windows.Forms.Label();
            this.label_storage = new System.Windows.Forms.Label();
            this.label_seasonStatus = new System.Windows.Forms.Label();
            this.label_money = new System.Windows.Forms.Label();
            this.label_moneyStatus = new System.Windows.Forms.Label();
            this.button_field1 = new System.Windows.Forms.Button();
            this.button_field2 = new System.Windows.Forms.Button();
            this.button_field3 = new System.Windows.Forms.Button();
            this.button_field4 = new System.Windows.Forms.Button();
            this.button_field5 = new System.Windows.Forms.Button();
            this.button_field6 = new System.Windows.Forms.Button();
            this.label_PlayerName = new System.Windows.Forms.Label();
            this.label_PlayerNameDisplay = new System.Windows.Forms.Label();
            this.label_FarmName = new System.Windows.Forms.Label();
            this.label_FarmNameDisplay = new System.Windows.Forms.Label();
            this.button_NextTurn = new System.Windows.Forms.Button();
            this.label_YearDisplay = new System.Windows.Forms.Label();
            this.label_YearLabel = new System.Windows.Forms.Label();
            this.label_DebtDisplay = new System.Windows.Forms.Label();
            this.label_Debt = new System.Windows.Forms.Label();
            this.toolStripSplitButton_Market = new System.Windows.Forms.ToolStripSplitButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSplitButton_Market,
            this.toolStripSplitButton_Storage,
            this.toolStripSplitButton_bank,
            this.toolStripButton_stats,
            this.toolStripButton_save,
            this.toolStripButton3});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1087, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSplitButton_Storage
            // 
            this.toolStripSplitButton_Storage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripSplitButton_Storage.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.storageStatusToolStripMenuItem,
            this.destroyCropToolStripMenuItem});
            this.toolStripSplitButton_Storage.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton_Storage.Image")));
            this.toolStripSplitButton_Storage.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton_Storage.Name = "toolStripSplitButton_Storage";
            this.toolStripSplitButton_Storage.Size = new System.Drawing.Size(63, 22);
            this.toolStripSplitButton_Storage.Text = "Storage";
            this.toolStripSplitButton_Storage.ButtonClick += new System.EventHandler(this.toolStripSplitButton_Storage_ButtonClick);
            // 
            // storageStatusToolStripMenuItem
            // 
            this.storageStatusToolStripMenuItem.Name = "storageStatusToolStripMenuItem";
            this.storageStatusToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.storageStatusToolStripMenuItem.Text = "Storage Status";
            this.storageStatusToolStripMenuItem.Click += new System.EventHandler(this.storageStatusToolStripMenuItem_Click);
            // 
            // destroyCropToolStripMenuItem
            // 
            this.destroyCropToolStripMenuItem.Name = "destroyCropToolStripMenuItem";
            this.destroyCropToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.destroyCropToolStripMenuItem.Text = "Buy Storage";
            this.destroyCropToolStripMenuItem.Click += new System.EventHandler(this.destroyCropToolStripMenuItem_Click);
            // 
            // toolStripSplitButton_bank
            // 
            this.toolStripSplitButton_bank.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripSplitButton_bank.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.takeLoanToolStripMenuItem,
            this.payDebtToolStripMenuItem});
            this.toolStripSplitButton_bank.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton_bank.Image")));
            this.toolStripSplitButton_bank.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton_bank.Name = "toolStripSplitButton_bank";
            this.toolStripSplitButton_bank.Size = new System.Drawing.Size(49, 22);
            this.toolStripSplitButton_bank.Text = "Bank";
            this.toolStripSplitButton_bank.ButtonClick += new System.EventHandler(this.toolStripSplitButton_bank_ButtonClick);
            // 
            // takeLoanToolStripMenuItem
            // 
            this.takeLoanToolStripMenuItem.Name = "takeLoanToolStripMenuItem";
            this.takeLoanToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.takeLoanToolStripMenuItem.Text = "Take Loan";
            this.takeLoanToolStripMenuItem.Click += new System.EventHandler(this.takeLoanToolStripMenuItem_Click);
            // 
            // payDebtToolStripMenuItem
            // 
            this.payDebtToolStripMenuItem.Name = "payDebtToolStripMenuItem";
            this.payDebtToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.payDebtToolStripMenuItem.Text = "Pay Debt";
            this.payDebtToolStripMenuItem.Click += new System.EventHandler(this.payDebtToolStripMenuItem_Click);
            // 
            // toolStripButton_stats
            // 
            this.toolStripButton_stats.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_stats.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_stats.Image")));
            this.toolStripButton_stats.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_stats.Name = "toolStripButton_stats";
            this.toolStripButton_stats.Size = new System.Drawing.Size(36, 22);
            this.toolStripButton_stats.Text = "Stats";
            this.toolStripButton_stats.Click += new System.EventHandler(this.toolStripButton_stats_Click);
            // 
            // toolStripButton_save
            // 
            this.toolStripButton_save.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton_save.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton_save.Image")));
            this.toolStripButton_save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_save.Name = "toolStripButton_save";
            this.toolStripButton_save.Size = new System.Drawing.Size(35, 22);
            this.toolStripButton_save.Text = "Save";
            this.toolStripButton_save.Click += new System.EventHandler(this.toolStripButton_save_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(34, 22);
            this.toolStripButton3.Text = "Quit";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // label_week
            // 
            this.label_week.AutoSize = true;
            this.label_week.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_week.Location = new System.Drawing.Point(36, 136);
            this.label_week.Name = "label_week";
            this.label_week.Size = new System.Drawing.Size(59, 20);
            this.label_week.TabIndex = 1;
            this.label_week.Text = "Week:";
            this.label_week.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_weekNumber
            // 
            this.label_weekNumber.AutoSize = true;
            this.label_weekNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_weekNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label_weekNumber.Location = new System.Drawing.Point(96, 136);
            this.label_weekNumber.Name = "label_weekNumber";
            this.label_weekNumber.Size = new System.Drawing.Size(122, 20);
            this.label_weekNumber.TabIndex = 2;
            this.label_weekNumber.Text = "Week_Display";
            this.label_weekNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_weekNumber.Click += new System.EventHandler(this.label_weekNumber_Click);
            // 
            // label_weather
            // 
            this.label_weather.AutoSize = true;
            this.label_weather.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_weather.Location = new System.Drawing.Point(13, 156);
            this.label_weather.Name = "label_weather";
            this.label_weather.Size = new System.Drawing.Size(82, 20);
            this.label_weather.TabIndex = 3;
            this.label_weather.Text = "Weather:";
            this.label_weather.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_weatherStatus
            // 
            this.label_weatherStatus.AutoSize = true;
            this.label_weatherStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_weatherStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label_weatherStatus.Location = new System.Drawing.Point(96, 156);
            this.label_weatherStatus.Name = "label_weatherStatus";
            this.label_weatherStatus.Size = new System.Drawing.Size(145, 20);
            this.label_weatherStatus.TabIndex = 4;
            this.label_weatherStatus.Text = "Weahter_Display";
            this.label_weatherStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_weatherStatus.Click += new System.EventHandler(this.label_weatherStatus_Click);
            // 
            // label_storage
            // 
            this.label_storage.AutoSize = true;
            this.label_storage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_storage.Location = new System.Drawing.Point(22, 116);
            this.label_storage.Name = "label_storage";
            this.label_storage.Size = new System.Drawing.Size(75, 20);
            this.label_storage.TabIndex = 5;
            this.label_storage.Text = "Season:";
            this.label_storage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_seasonStatus
            // 
            this.label_seasonStatus.AutoSize = true;
            this.label_seasonStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_seasonStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label_seasonStatus.Location = new System.Drawing.Point(96, 116);
            this.label_seasonStatus.Name = "label_seasonStatus";
            this.label_seasonStatus.Size = new System.Drawing.Size(138, 20);
            this.label_seasonStatus.TabIndex = 6;
            this.label_seasonStatus.Text = "Season_Display";
            this.label_seasonStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_seasonStatus.Click += new System.EventHandler(this.label_seasonStatus_Click);
            // 
            // label_money
            // 
            this.label_money.AutoSize = true;
            this.label_money.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_money.Location = new System.Drawing.Point(29, 25);
            this.label_money.Name = "label_money";
            this.label_money.Size = new System.Drawing.Size(66, 20);
            this.label_money.TabIndex = 7;
            this.label_money.Text = "Money:";
            this.label_money.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_moneyStatus
            // 
            this.label_moneyStatus.AutoSize = true;
            this.label_moneyStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_moneyStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label_moneyStatus.Location = new System.Drawing.Point(95, 25);
            this.label_moneyStatus.Name = "label_moneyStatus";
            this.label_moneyStatus.Size = new System.Drawing.Size(129, 20);
            this.label_moneyStatus.TabIndex = 8;
            this.label_moneyStatus.Text = "Money_Display";
            this.label_moneyStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_field1
            // 
            this.button_field1.BackColor = System.Drawing.SystemColors.Control;
            this.button_field1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_field1.Location = new System.Drawing.Point(12, 184);
            this.button_field1.Name = "button_field1";
            this.button_field1.Size = new System.Drawing.Size(172, 309);
            this.button_field1.TabIndex = 10;
            this.button_field1.Text = "Field 1";
            this.button_field1.UseVisualStyleBackColor = false;
            this.button_field1.Click += new System.EventHandler(this.button_field1_Click);
            // 
            // button_field2
            // 
            this.button_field2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_field2.Location = new System.Drawing.Point(190, 184);
            this.button_field2.Name = "button_field2";
            this.button_field2.Size = new System.Drawing.Size(172, 309);
            this.button_field2.TabIndex = 11;
            this.button_field2.Text = "Field 2";
            this.button_field2.UseVisualStyleBackColor = true;
            this.button_field2.Click += new System.EventHandler(this.button_field2_Click);
            // 
            // button_field3
            // 
            this.button_field3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_field3.Location = new System.Drawing.Point(368, 184);
            this.button_field3.Name = "button_field3";
            this.button_field3.Size = new System.Drawing.Size(172, 309);
            this.button_field3.TabIndex = 12;
            this.button_field3.Text = "Field 3";
            this.button_field3.UseVisualStyleBackColor = true;
            this.button_field3.Click += new System.EventHandler(this.button_field3_Click);
            // 
            // button_field4
            // 
            this.button_field4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_field4.Location = new System.Drawing.Point(546, 184);
            this.button_field4.Name = "button_field4";
            this.button_field4.Size = new System.Drawing.Size(172, 309);
            this.button_field4.TabIndex = 13;
            this.button_field4.Text = "Field 4";
            this.button_field4.UseVisualStyleBackColor = true;
            this.button_field4.Click += new System.EventHandler(this.button_field4_Click);
            // 
            // button_field5
            // 
            this.button_field5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_field5.Location = new System.Drawing.Point(724, 184);
            this.button_field5.Name = "button_field5";
            this.button_field5.Size = new System.Drawing.Size(172, 309);
            this.button_field5.TabIndex = 14;
            this.button_field5.Text = "Field 5";
            this.button_field5.UseVisualStyleBackColor = true;
            this.button_field5.Click += new System.EventHandler(this.button_field5_Click);
            // 
            // button_field6
            // 
            this.button_field6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_field6.Location = new System.Drawing.Point(902, 184);
            this.button_field6.Name = "button_field6";
            this.button_field6.Size = new System.Drawing.Size(172, 309);
            this.button_field6.TabIndex = 15;
            this.button_field6.Text = "Field 6";
            this.button_field6.UseVisualStyleBackColor = true;
            this.button_field6.Click += new System.EventHandler(this.button_field6_Click);
            // 
            // label_PlayerName
            // 
            this.label_PlayerName.AutoSize = true;
            this.label_PlayerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_PlayerName.Location = new System.Drawing.Point(364, 25);
            this.label_PlayerName.Name = "label_PlayerName";
            this.label_PlayerName.Size = new System.Drawing.Size(114, 20);
            this.label_PlayerName.TabIndex = 16;
            this.label_PlayerName.Text = "Player Name:";
            this.label_PlayerName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_PlayerNameDisplay
            // 
            this.label_PlayerNameDisplay.AutoSize = true;
            this.label_PlayerNameDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_PlayerNameDisplay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label_PlayerNameDisplay.Location = new System.Drawing.Point(484, 25);
            this.label_PlayerNameDisplay.Name = "label_PlayerNameDisplay";
            this.label_PlayerNameDisplay.Size = new System.Drawing.Size(19, 20);
            this.label_PlayerNameDisplay.TabIndex = 17;
            this.label_PlayerNameDisplay.Text = "_";
            this.label_PlayerNameDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_FarmName
            // 
            this.label_FarmName.AutoSize = true;
            this.label_FarmName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_FarmName.Location = new System.Drawing.Point(720, 25);
            this.label_FarmName.Name = "label_FarmName";
            this.label_FarmName.Size = new System.Drawing.Size(106, 20);
            this.label_FarmName.TabIndex = 18;
            this.label_FarmName.Text = "Farm Name:";
            this.label_FarmName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_FarmNameDisplay
            // 
            this.label_FarmNameDisplay.AutoSize = true;
            this.label_FarmNameDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_FarmNameDisplay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label_FarmNameDisplay.Location = new System.Drawing.Point(832, 25);
            this.label_FarmNameDisplay.Name = "label_FarmNameDisplay";
            this.label_FarmNameDisplay.Size = new System.Drawing.Size(19, 20);
            this.label_FarmNameDisplay.TabIndex = 19;
            this.label_FarmNameDisplay.Text = "_";
            this.label_FarmNameDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_NextTurn
            // 
            this.button_NextTurn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_NextTurn.Location = new System.Drawing.Point(12, 516);
            this.button_NextTurn.Name = "button_NextTurn";
            this.button_NextTurn.Size = new System.Drawing.Size(1062, 111);
            this.button_NextTurn.TabIndex = 20;
            this.button_NextTurn.Text = "Next Turn";
            this.button_NextTurn.UseVisualStyleBackColor = true;
            this.button_NextTurn.Click += new System.EventHandler(this.button_NextTurn_Click);
            // 
            // label_YearDisplay
            // 
            this.label_YearDisplay.AutoSize = true;
            this.label_YearDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_YearDisplay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label_YearDisplay.Location = new System.Drawing.Point(96, 96);
            this.label_YearDisplay.Name = "label_YearDisplay";
            this.label_YearDisplay.Size = new System.Drawing.Size(115, 20);
            this.label_YearDisplay.TabIndex = 22;
            this.label_YearDisplay.Text = "Year_Display";
            this.label_YearDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_YearDisplay.Click += new System.EventHandler(this.label_YearDisplay_Click);
            // 
            // label_YearLabel
            // 
            this.label_YearLabel.AutoSize = true;
            this.label_YearLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_YearLabel.Location = new System.Drawing.Point(43, 96);
            this.label_YearLabel.Name = "label_YearLabel";
            this.label_YearLabel.Size = new System.Drawing.Size(52, 20);
            this.label_YearLabel.TabIndex = 21;
            this.label_YearLabel.Text = "Year:";
            this.label_YearLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_YearLabel.Click += new System.EventHandler(this.label_YearLabel_Click);
            // 
            // label_DebtDisplay
            // 
            this.label_DebtDisplay.AutoSize = true;
            this.label_DebtDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_DebtDisplay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label_DebtDisplay.Location = new System.Drawing.Point(95, 45);
            this.label_DebtDisplay.Name = "label_DebtDisplay";
            this.label_DebtDisplay.Size = new System.Drawing.Size(116, 20);
            this.label_DebtDisplay.TabIndex = 24;
            this.label_DebtDisplay.Text = "Debt_Display";
            this.label_DebtDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_Debt
            // 
            this.label_Debt.AutoSize = true;
            this.label_Debt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Debt.Location = new System.Drawing.Point(42, 45);
            this.label_Debt.Name = "label_Debt";
            this.label_Debt.Size = new System.Drawing.Size(53, 20);
            this.label_Debt.TabIndex = 23;
            this.label_Debt.Text = "Debt:";
            this.label_Debt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // toolStripSplitButton_Market
            // 
            this.toolStripSplitButton_Market.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripSplitButton_Market.DropDownButtonWidth = 0;
            this.toolStripSplitButton_Market.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSplitButton_Market.Image")));
            this.toolStripSplitButton_Market.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton_Market.Name = "toolStripSplitButton_Market";
            this.toolStripSplitButton_Market.Size = new System.Drawing.Size(49, 22);
            this.toolStripSplitButton_Market.Text = "Market";
            this.toolStripSplitButton_Market.ButtonClick += new System.EventHandler(this.toolStripSplitButton_Market_ButtonClick);
            // 
            // Main_Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1087, 639);
            this.Controls.Add(this.label_DebtDisplay);
            this.Controls.Add(this.label_Debt);
            this.Controls.Add(this.label_YearDisplay);
            this.Controls.Add(this.label_YearLabel);
            this.Controls.Add(this.button_NextTurn);
            this.Controls.Add(this.label_FarmNameDisplay);
            this.Controls.Add(this.label_FarmName);
            this.Controls.Add(this.label_PlayerNameDisplay);
            this.Controls.Add(this.label_PlayerName);
            this.Controls.Add(this.button_field6);
            this.Controls.Add(this.button_field5);
            this.Controls.Add(this.button_field4);
            this.Controls.Add(this.button_field3);
            this.Controls.Add(this.button_field2);
            this.Controls.Add(this.button_field1);
            this.Controls.Add(this.label_moneyStatus);
            this.Controls.Add(this.label_money);
            this.Controls.Add(this.label_seasonStatus);
            this.Controls.Add(this.label_storage);
            this.Controls.Add(this.label_weatherStatus);
            this.Controls.Add(this.label_weather);
            this.Controls.Add(this.label_weekNumber);
            this.Controls.Add(this.label_week);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Main_Menu";
            this.Text = "Farm Manager Menu";
            this.Load += new System.EventHandler(this.Main_Menu_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton_Storage;
        private System.Windows.Forms.ToolStripMenuItem destroyCropToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton_save;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.Label label_week;
        private System.Windows.Forms.Label label_weekNumber;
        private System.Windows.Forms.Label label_weather;
        private System.Windows.Forms.Label label_weatherStatus;
        private System.Windows.Forms.Label label_storage;
        private System.Windows.Forms.Label label_seasonStatus;
        private System.Windows.Forms.Label label_money;
        private System.Windows.Forms.Label label_moneyStatus;
        private System.Windows.Forms.ToolStripMenuItem storageStatusToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton_stats;
        private System.Windows.Forms.Button button_field1;
        private System.Windows.Forms.Button button_field2;
        private System.Windows.Forms.Button button_field3;
        private System.Windows.Forms.Button button_field4;
        private System.Windows.Forms.Button button_field5;
        private System.Windows.Forms.Button button_field6;
        private System.Windows.Forms.Label label_PlayerName;
        private System.Windows.Forms.Label label_PlayerNameDisplay;
        private System.Windows.Forms.Label label_FarmName;
        private System.Windows.Forms.Label label_FarmNameDisplay;
        private System.Windows.Forms.Button button_NextTurn;
        private System.Windows.Forms.Label label_YearDisplay;
        private System.Windows.Forms.Label label_YearLabel;
        private System.Windows.Forms.Label label_DebtDisplay;
        private System.Windows.Forms.Label label_Debt;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton_bank;
        private System.Windows.Forms.ToolStripMenuItem takeLoanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem payDebtToolStripMenuItem;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton_Market;
    }
}