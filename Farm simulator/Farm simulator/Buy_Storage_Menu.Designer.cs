﻿namespace Farm_simulator
{
    partial class Buy_Storage_Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Buy_Storage_Menu));
            this.button_buyVegStorage = new System.Windows.Forms.Button();
            this.button_buyFruitStorage = new System.Windows.Forms.Button();
            this.button_buyCerealStorage = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label_PriceDisplay = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_buyVegStorage
            // 
            this.button_buyVegStorage.Location = new System.Drawing.Point(12, 66);
            this.button_buyVegStorage.Name = "button_buyVegStorage";
            this.button_buyVegStorage.Size = new System.Drawing.Size(189, 23);
            this.button_buyVegStorage.TabIndex = 0;
            this.button_buyVegStorage.Text = "Buy Vegetable Storage";
            this.button_buyVegStorage.UseVisualStyleBackColor = true;
            this.button_buyVegStorage.Click += new System.EventHandler(this.button_buyVegStorage_Click);
            // 
            // button_buyFruitStorage
            // 
            this.button_buyFruitStorage.Location = new System.Drawing.Point(12, 95);
            this.button_buyFruitStorage.Name = "button_buyFruitStorage";
            this.button_buyFruitStorage.Size = new System.Drawing.Size(189, 23);
            this.button_buyFruitStorage.TabIndex = 1;
            this.button_buyFruitStorage.Text = "Buy Fruit Storage";
            this.button_buyFruitStorage.UseVisualStyleBackColor = true;
            this.button_buyFruitStorage.Click += new System.EventHandler(this.button_buyFruitStorage_Click);
            // 
            // button_buyCerealStorage
            // 
            this.button_buyCerealStorage.Location = new System.Drawing.Point(12, 124);
            this.button_buyCerealStorage.Name = "button_buyCerealStorage";
            this.button_buyCerealStorage.Size = new System.Drawing.Size(189, 23);
            this.button_buyCerealStorage.TabIndex = 2;
            this.button_buyCerealStorage.Text = "Buy Cereal Storage";
            this.button_buyCerealStorage.UseVisualStyleBackColor = true;
            this.button_buyCerealStorage.Click += new System.EventHandler(this.button_buyCerealStorage_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(27, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Price:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label_PriceDisplay
            // 
            this.label_PriceDisplay.AutoSize = true;
            this.label_PriceDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_PriceDisplay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label_PriceDisplay.Location = new System.Drawing.Point(83, 25);
            this.label_PriceDisplay.Name = "label_PriceDisplay";
            this.label_PriceDisplay.Size = new System.Drawing.Size(107, 17);
            this.label_PriceDisplay.TabIndex = 4;
            this.label_PriceDisplay.Text = "Price_Display";
            this.label_PriceDisplay.Click += new System.EventHandler(this.label2_Click);
            // 
            // Buy_Storage_Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(213, 156);
            this.Controls.Add(this.label_PriceDisplay);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_buyCerealStorage);
            this.Controls.Add(this.button_buyFruitStorage);
            this.Controls.Add(this.button_buyVegStorage);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Buy_Storage_Menu";
            this.Text = "Buy Storage";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_buyVegStorage;
        private System.Windows.Forms.Button button_buyFruitStorage;
        private System.Windows.Forms.Button button_buyCerealStorage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_PriceDisplay;
    }
}