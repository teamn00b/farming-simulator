﻿namespace Farm_simulator
{
    partial class Field_Information_Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Field_Information_Menu));
            this.label_fieldName = new System.Windows.Forms.Label();
            this.label_cropPlanted = new System.Windows.Forms.Label();
            this.label_weeksLeft = new System.Windows.Forms.Label();
            this.label_quality = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button_applyFertiliser = new System.Windows.Forms.Button();
            this.PlantVeg = new System.Windows.Forms.Button();
            this.button_destroyCrop = new System.Windows.Forms.Button();
            this.button_harvestCrop = new System.Windows.Forms.Button();
            this.label_field = new System.Windows.Forms.Label();
            this.button_ApplyPesticide = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.CropValue = new System.Windows.Forms.Label();
            this.GrowthValue = new System.Windows.Forms.Label();
            this.QualityValue = new System.Windows.Forms.Label();
            this.StatusValue = new System.Windows.Forms.Label();
            this.InfectedValue = new System.Windows.Forms.Label();
            this.FertiliserValue = new System.Windows.Forms.Label();
            this.PlantFruit = new System.Windows.Forms.Button();
            this.PlantCereal = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_fieldName
            // 
            this.label_fieldName.AutoSize = true;
            this.label_fieldName.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_fieldName.Location = new System.Drawing.Point(68, 9);
            this.label_fieldName.Name = "label_fieldName";
            this.label_fieldName.Size = new System.Drawing.Size(23, 22);
            this.label_fieldName.TabIndex = 0;
            this.label_fieldName.Text = "X";
            this.label_fieldName.Click += new System.EventHandler(this.label_fieldName_Click);
            // 
            // label_cropPlanted
            // 
            this.label_cropPlanted.AutoSize = true;
            this.label_cropPlanted.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_cropPlanted.Location = new System.Drawing.Point(41, 59);
            this.label_cropPlanted.Name = "label_cropPlanted";
            this.label_cropPlanted.Size = new System.Drawing.Size(47, 17);
            this.label_cropPlanted.TabIndex = 1;
            this.label_cropPlanted.Text = "Crop:";
            this.label_cropPlanted.Click += new System.EventHandler(this.label_cropPlanted_Click);
            // 
            // label_weeksLeft
            // 
            this.label_weeksLeft.AutoSize = true;
            this.label_weeksLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_weeksLeft.Location = new System.Drawing.Point(27, 88);
            this.label_weeksLeft.Name = "label_weeksLeft";
            this.label_weeksLeft.Size = new System.Drawing.Size(64, 17);
            this.label_weeksLeft.TabIndex = 2;
            this.label_weeksLeft.Text = "Growth:";
            // 
            // label_quality
            // 
            this.label_quality.AutoSize = true;
            this.label_quality.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_quality.Location = new System.Drawing.Point(28, 117);
            this.label_quality.Name = "label_quality";
            this.label_quality.Size = new System.Drawing.Size(64, 17);
            this.label_quality.TabIndex = 3;
            this.label_quality.Text = "Quality:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(199, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 22);
            this.label1.TabIndex = 4;
            this.label1.Text = "Status:";
            // 
            // button_applyFertiliser
            // 
            this.button_applyFertiliser.Location = new System.Drawing.Point(203, 125);
            this.button_applyFertiliser.Name = "button_applyFertiliser";
            this.button_applyFertiliser.Size = new System.Drawing.Size(120, 60);
            this.button_applyFertiliser.TabIndex = 5;
            this.button_applyFertiliser.Text = "Apply Fertiliser";
            this.button_applyFertiliser.UseVisualStyleBackColor = true;
            this.button_applyFertiliser.Click += new System.EventHandler(this.button_applyFertiliser_Click);
            // 
            // PlantVeg
            // 
            this.PlantVeg.Location = new System.Drawing.Point(203, 60);
            this.PlantVeg.Name = "PlantVeg";
            this.PlantVeg.Size = new System.Drawing.Size(120, 59);
            this.PlantVeg.TabIndex = 6;
            this.PlantVeg.Text = "Plant Vegatable";
            this.PlantVeg.UseVisualStyleBackColor = true;
            this.PlantVeg.Click += new System.EventHandler(this.button_sowCrop_Click);
            // 
            // button_destroyCrop
            // 
            this.button_destroyCrop.Location = new System.Drawing.Point(455, 126);
            this.button_destroyCrop.Name = "button_destroyCrop";
            this.button_destroyCrop.Size = new System.Drawing.Size(120, 59);
            this.button_destroyCrop.TabIndex = 7;
            this.button_destroyCrop.Text = "Destroy Crop";
            this.button_destroyCrop.UseVisualStyleBackColor = true;
            this.button_destroyCrop.Click += new System.EventHandler(this.button_destroyCrop_Click);
            // 
            // button_harvestCrop
            // 
            this.button_harvestCrop.Location = new System.Drawing.Point(203, 191);
            this.button_harvestCrop.Name = "button_harvestCrop";
            this.button_harvestCrop.Size = new System.Drawing.Size(372, 59);
            this.button_harvestCrop.TabIndex = 8;
            this.button_harvestCrop.Text = "Harvest Crop";
            this.button_harvestCrop.UseVisualStyleBackColor = true;
            this.button_harvestCrop.Click += new System.EventHandler(this.button_harvestCrop_Click);
            // 
            // label_field
            // 
            this.label_field.AutoSize = true;
            this.label_field.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_field.Location = new System.Drawing.Point(12, 9);
            this.label_field.Name = "label_field";
            this.label_field.Size = new System.Drawing.Size(60, 22);
            this.label_field.TabIndex = 9;
            this.label_field.Text = "Field:";
            // 
            // button_ApplyPesticide
            // 
            this.button_ApplyPesticide.Location = new System.Drawing.Point(329, 125);
            this.button_ApplyPesticide.Name = "button_ApplyPesticide";
            this.button_ApplyPesticide.Size = new System.Drawing.Size(120, 60);
            this.button_ApplyPesticide.TabIndex = 11;
            this.button_ApplyPesticide.Text = "Apply Pesticide";
            this.button_ApplyPesticide.UseVisualStyleBackColor = true;
            this.button_ApplyPesticide.Click += new System.EventHandler(this.button_ApplyPesticide_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 177);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "Fertilised:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(21, 147);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "Infected:";
            // 
            // CropValue
            // 
            this.CropValue.AutoSize = true;
            this.CropValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CropValue.Location = new System.Drawing.Point(98, 59);
            this.CropValue.Name = "CropValue";
            this.CropValue.Size = new System.Drawing.Size(49, 17);
            this.CropValue.TabIndex = 14;
            this.CropValue.Text = "Value";
            // 
            // GrowthValue
            // 
            this.GrowthValue.AutoSize = true;
            this.GrowthValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrowthValue.Location = new System.Drawing.Point(97, 88);
            this.GrowthValue.Name = "GrowthValue";
            this.GrowthValue.Size = new System.Drawing.Size(49, 17);
            this.GrowthValue.TabIndex = 15;
            this.GrowthValue.Text = "Value";
            // 
            // QualityValue
            // 
            this.QualityValue.AutoSize = true;
            this.QualityValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QualityValue.Location = new System.Drawing.Point(98, 117);
            this.QualityValue.Name = "QualityValue";
            this.QualityValue.Size = new System.Drawing.Size(49, 17);
            this.QualityValue.TabIndex = 16;
            this.QualityValue.Text = "Value";
            // 
            // StatusValue
            // 
            this.StatusValue.AutoSize = true;
            this.StatusValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusValue.Location = new System.Drawing.Point(278, 9);
            this.StatusValue.Name = "StatusValue";
            this.StatusValue.Size = new System.Drawing.Size(61, 22);
            this.StatusValue.TabIndex = 17;
            this.StatusValue.Text = "Value";
            this.StatusValue.Click += new System.EventHandler(this.StatusValue_Click);
            // 
            // InfectedValue
            // 
            this.InfectedValue.AutoSize = true;
            this.InfectedValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InfectedValue.Location = new System.Drawing.Point(98, 147);
            this.InfectedValue.Name = "InfectedValue";
            this.InfectedValue.Size = new System.Drawing.Size(49, 17);
            this.InfectedValue.TabIndex = 18;
            this.InfectedValue.Text = "Value";
            // 
            // FertiliserValue
            // 
            this.FertiliserValue.AutoSize = true;
            this.FertiliserValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FertiliserValue.Location = new System.Drawing.Point(98, 177);
            this.FertiliserValue.Name = "FertiliserValue";
            this.FertiliserValue.Size = new System.Drawing.Size(49, 17);
            this.FertiliserValue.TabIndex = 19;
            this.FertiliserValue.Text = "Value";
            // 
            // PlantFruit
            // 
            this.PlantFruit.Location = new System.Drawing.Point(329, 60);
            this.PlantFruit.Name = "PlantFruit";
            this.PlantFruit.Size = new System.Drawing.Size(120, 59);
            this.PlantFruit.TabIndex = 20;
            this.PlantFruit.Text = "Plant Fruit";
            this.PlantFruit.UseVisualStyleBackColor = true;
            this.PlantFruit.Click += new System.EventHandler(this.PlantFruit_Click);
            // 
            // PlantCereal
            // 
            this.PlantCereal.Location = new System.Drawing.Point(455, 60);
            this.PlantCereal.Name = "PlantCereal";
            this.PlantCereal.Size = new System.Drawing.Size(120, 59);
            this.PlantCereal.TabIndex = 21;
            this.PlantCereal.Text = "Plant Cereal";
            this.PlantCereal.UseVisualStyleBackColor = true;
            this.PlantCereal.Click += new System.EventHandler(this.PlantCereal_Click);
            // 
            // Field_Information_Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(587, 262);
            this.Controls.Add(this.PlantCereal);
            this.Controls.Add(this.PlantFruit);
            this.Controls.Add(this.FertiliserValue);
            this.Controls.Add(this.InfectedValue);
            this.Controls.Add(this.StatusValue);
            this.Controls.Add(this.QualityValue);
            this.Controls.Add(this.GrowthValue);
            this.Controls.Add(this.CropValue);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button_ApplyPesticide);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label_field);
            this.Controls.Add(this.button_harvestCrop);
            this.Controls.Add(this.button_destroyCrop);
            this.Controls.Add(this.PlantVeg);
            this.Controls.Add(this.button_applyFertiliser);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_quality);
            this.Controls.Add(this.label_weeksLeft);
            this.Controls.Add(this.label_cropPlanted);
            this.Controls.Add(this.label_fieldName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Field_Information_Menu";
            this.Text = "Field Manager";
            this.Load += new System.EventHandler(this.Field_Information_Menu_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_fieldName;
        private System.Windows.Forms.Label label_cropPlanted;
        private System.Windows.Forms.Label label_weeksLeft;
        private System.Windows.Forms.Label label_quality;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_applyFertiliser;
        private System.Windows.Forms.Button PlantVeg;
        private System.Windows.Forms.Button button_destroyCrop;
        private System.Windows.Forms.Button button_harvestCrop;
        private System.Windows.Forms.Label label_field;
        private System.Windows.Forms.Button button_ApplyPesticide;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label CropValue;
        private System.Windows.Forms.Label GrowthValue;
        private System.Windows.Forms.Label QualityValue;
        private System.Windows.Forms.Label StatusValue;
        private System.Windows.Forms.Label InfectedValue;
        private System.Windows.Forms.Label FertiliserValue;
        private System.Windows.Forms.Button PlantFruit;
        private System.Windows.Forms.Button PlantCereal;
    }
}